package com.omelchenkoaleks.criminalintent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CrimeListFragment extends Fragment {

    private RecyclerView mCrimeRecyclerView;
    private CrimeAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // вызываем этот метод, чтобы явно заполнить представление макета фрагмента
        // третий параметр false потому-что представление будет добавлено
        // в коде активности, а не здесь
        View view = inflater.inflate(R.layout.fragment_crime_list,
                container, false);

        // ПОДКЛЮЧЕНИЕ ВИДЖЕТОВ во фрагменте
        mCrimeRecyclerView = view.findViewById(R.id.crime_recycler_view);

        // виджету RecyclerView назначается объект LayoutManager - это
        // необходимо для работы RecyclerView = layoutManager управляет
        // позиционированием элементов, а также определяет поведение прокрутки
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }

    // метод настраивает пользовательский интерфейс
    private void updateUI() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        mAdapter = new CrimeAdapter(crimes);
        mCrimeRecyclerView.setAdapter(mAdapter);
    }

    // реализуем ViewHolder - в нем хранится ссылка на все представление View
    private class CrimeHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private Crime mCrime;

        private TextView mTitleTextView;
        private TextView mDateTextView;

        // в этом конструкторе происходит заполнение list_item_crime.xml
        // вызов передается напрямую конструктору ViewHolder
        public CrimeHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_crime, parent, false));
            // для itemView - представления View всей строки - CrimeHolder
            // назначается получателем событий щелчка
            itemView.setOnClickListener(this);

            mTitleTextView = itemView.findViewById(R.id.crime_title);
            mDateTextView = itemView.findViewById(R.id.crime_date);
        }

        // вызывается каждый раз, когда в CrimeHolder должен отображаться
        // новый объект Crime
        public void bind(Crime crime) {
            // получив объект Crime CrimeHolder обновит виджеты TextView
            // с описанием и датой в соответствии с состоянием Crime
            mCrime = crime;
            mTitleTextView.setText(mCrime.getTitle());
            mDateTextView.setText(mCrime.getDate().toString());
        }

        @Override
        public void onClick(View view) {
            // запускаем активность из фрагмента
//            Intent intent = new Intent(getActivity(), CrimeActivity.class);
            // обновляем с использование созданного метода с передачей ID ресурса
            Intent intent = CrimeActivity.newIntent(getActivity(), mCrime.getUUID());
            startActivity(intent);
        }
    }

    // реализуем адаптер - отвечает за создание объектов ViewHolder и
    // связывает ViewHolder с данными из уровня модели
    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {

        private List<Crime> mCrimes;

        public CrimeAdapter(List<Crime> crimes) {
            mCrimes = crimes;
        }

        // метод вызывается виджетом RecycleView, когда ему требуется
        // новое представление для отображения элемента
        @NonNull
        @Override
        public CrimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // создаем объект LayoutInflater и используем его
            // для создания нового объекта CrimeHolder
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new CrimeHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull CrimeHolder holder, int position) {
            Crime crime = mCrimes.get(position);
            // метод вызывается для связи объекта CrimeHolder
            // с объектом конкретного преступления
            holder.bind(crime);
        }

        @Override
        public int getItemCount() {
            return mCrimes.size();
        }
    }
}
