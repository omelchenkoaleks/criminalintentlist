package com.omelchenkoaleks.criminalintent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

public class CrimeFragment extends Fragment {

    // закрыта, потому что другим фрагментам
    // не понадобится работать с этим дополнением
    private static final String ARG_CRIME_ID = "crime_id";

    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;

    public static CrimeFragment newInstance(UUID crimeId) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeId);

        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    // экземпляр фрагмента настраивается в этом методе
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID crimeId = (UUID) getArguments().getSerializable(ARG_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
    }

    // в этом методе заполняется макет представления фрагмента,
    // а заполненный объект View возвращается активности-хосту
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // вызываем этот метод, чтобы явно заполнить представление макета фрагмента
        // третий параметр false потому-что представление будет добавлено
        // в коде активности, а не здесь
        View view = inflater.inflate(R.layout.fragment_crime, container, false);


        // ПОДКЛЮЧЕНИЕ ВИДЖЕТОВ во фрагменте

        // EditText
        mTitleField = view.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getTitle());

        // при назначении слушателя мы создаем анонимный класс,
        // который реализует интерфейс слушателя TextWatcher (его три метода)
        mTitleField.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence,
                                          int start, int count, int after) {
                // TODO:
            }

            @Override
            public void onTextChanged(CharSequence charSequence,
                                      int start, int before, int count) {
                // метод toString() на объекте charSequence возвращает
                // строку, которая используется для задания заголовка Crime
                mCrime.setTitle(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // TODO:
            }
        });

        // Button
        mDateButton = view.findViewById(R.id.crime_date);
        mDateButton.setText(mCrime.getDate().toString());
        // блокируем кнопку = что гарантирует,
        // что она никак не отреагирует на нажатие
        mDateButton.setEnabled(false);

        // CheckBox
        mSolvedCheckBox = view.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(mCrime.isSolved());

        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });

        return view;
    }
}
