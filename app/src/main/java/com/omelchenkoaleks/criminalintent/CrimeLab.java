package com.omelchenkoaleks.criminalintent;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrimeLab {

    // префикс s у переменной используем для того,
    // чтобы показать, что переменная статическая
    private static CrimeLab sCrimeLab;

    private List<Crime> mCrimes;

    public static CrimeLab get(Context context) {

        if (sCrimeLab == null) {
            sCrimeLab = new CrimeLab(context);
        }
        return sCrimeLab;
    }

    // закрытый конструктор гарантирует, что другие классы
    // не смогут создать экземпляр CrimeLab в обход метода get()
    private CrimeLab(Context context) {
        mCrimes = new ArrayList<>();

        // пока временно генерируем текстовые объекты
        for (int i = 0; i < 100; i++) {
            Crime crime = new Crime();
            crime.setTitle("Crime #" + i);
            crime.setSolved(i % 2 == 0);
            mCrimes.add(crime);
        }
    }

    public List<Crime> getCrimes() {
        return mCrimes;
    }

    public Crime getCrime(UUID id) {

        for (Crime crime : mCrimes) {
            if (crime.getUUID().equals(id)) {
                return crime;
            }
        }
        return null;
    }
}
