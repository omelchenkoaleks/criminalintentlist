package com.omelchenkoaleks.criminalintent;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

public class CrimeActivity extends SingleFragmentActivity {

    private static final String EXTRA_CRIME_ID =
            "com.omelchenkoaleks.criminalintent.crime_id";

    //  после создания явного интента мы вызываем putExtra(),
    //  передавая строковый ключ и связанное с ним значение (crimeId)
    public static Intent newIntent(Context packageContext, UUID crimeID) {
        Intent intent = new Intent(packageContext, CrimeActivity.class);
        intent.putExtra(EXTRA_CRIME_ID, crimeID);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        // получаем id из дополнения
        UUID crimeId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_CRIME_ID);
        // создаем CrimeFragment
        return CrimeFragment.newInstance(crimeId);
    }
}
